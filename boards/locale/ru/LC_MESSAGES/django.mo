��    T      �  q   \            !     ?     G     S     [     c     j  	   q     {     �     �     �     �     �  %   �     �               "     /      5  !   V     x     �     �     �     �     �  	   �     �     �  '   �     "	  	   .	     8	     =	     D	  
   I	     T	     [	     i	     o	     w	     }	  
   �	     �	     �	  :   �	     �	     �	     
     
     7
     >
     B
     Q
  <   V
     �
     �
  &   �
     �
     �
     �
     �
  '   �
  n        �     �     �      �  "   �     �     �  :        M     T  	   ]     g     n     �     �     �     �  �  �  E   u     �     �  
   �     �     �          2  .   R     �  "   �     �  '   �     �  H     0   L  
   }     �     �     �  C   �  6     >   O     �  !   �  )   �  $   �       #   &  7   J     �  -   �     �     �               $  )   7     a  %   u     �     �  
   �     �     �     �       v     8   �     �  !   �  D   �     @     S  !   Z     |  �   �  	     
     A   !     c     �  3   �     �  K   �  �     
             .  K   H  S   �     �  F     j   H  
   �     �     �     �  !   �       ,   2     _  1   n     ?       3   )   J   L           4      T      M   
   '   N      Q   9             ;      ,       +   .   8          O   &   K   @   P   I   	      C   -          %   =   G               A           >   "          2   H                6          $             D   1       <          F                      B       0          R          5      #   :             7       !       (       *   /   S         E                 2 line breaks for a new line. Add tag All threads Archive Authors Ban IP Banned Bold text Captcha validation failed Comment Create new thread Delete Distributed under the Edit Either text or image must be entered. Enable moderation panel Feed First access: Gallery mode Image Image must be less than %s bytes Inappropriate characters in tags. Insert your user id above Italic text Last access: Last update:  Link to a post Login Next page No such user found No tags found. No threads exist. Create the first one! Normal mode Not found Open Pages: Post Post image Posts: Previous page Quote Replies Reply Reply to thread Repository Save Settings Skipped %(count)s replies. Open thread to see all replies. Speed: %(ppd)s posts per day Spoiler Strikethrough text Such image was already posted Syntax Tag Tag management Tags Tags must be delimited by spaces. Text or image is required. Tags: Text Text must have less than %s characters Text syntax Theme This page does not exist Title Title must have less than %s characters Type message here. You can reply to message >>123 like
 this. 2 new lines are required to start new paragraph. Up User ID User: Wait %s minutes after last login Wait %s seconds after last posting You are moderator. You need to new line before: Your IP address has been banned. Contact the administrator author designer developer images javascript developer license posts to bumplimit replies tag1 several_words_tag Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-22 13:07+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 2 перевода строки создают новый абзац. Добавить тег Все темы Архив Авторы Заблокировать IP Заблокирован Полужирный текст Проверка капчи провалена Комментарий Создать новую тему Удалить Распространяется под Изменить Текст или картинка должны быть введены. Включить панель модерации Лента Первый доступ: Режим галереи Изображение Изображение должно быть менее %s байт Недопустимые символы в тегах. Вставьте свой ID пользователя выше Курсивный текст Последний доступ:  Последнее обновление:  Ссылка на сообщение Вход Следующая страница Данный пользователь не найден Теги не найдены. Нет тем. Создайте первую! Нормальный режим Не найдено Открыть Страницы:  Отправить Изображение сообщения Сообщений: Предыдущая страница Цитата Ответы Ответ Ответить в тему Репозиторий Сохранить Настройки Пропущено %(count)s ответов. Откройте тред, чтобы увидеть все ответы. Скорость: %(ppd)s сообщений в день Спойлер Зачеркнутый текст Такое изображение уже было загружено Синтаксис Тег Управление тегами Теги Теги должны быть разделены пробелами. Текст или изображение обязательны. Теги: Текст Текст должен быть короче %s символов Синтаксис текста Тема Этой страницы не существует Заголовок Заголовок должен иметь меньше %s символов Введите сообщение здесь. Вы можете ответить на сообщение >>123 вот так. 2 переноса строки обязательны для создания нового абзаца. Вверх ID пользователя Пользователь: Подождите %s минут после последнего входа Подождите %s секунд после последнего постинга Вы модератор. Перед этими тегами нужна новая строка: Ваш IP адрес был заблокирован. Свяжитесь с администратором автор дизайнер разработчик изображений разработчик javascript лицензией сообщений до бамплимита ответов тег1 тег_из_нескольких_слов 